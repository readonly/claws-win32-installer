#!/bin/bash

# This is a helper script for cleanly rebuilding packages.  It deletes build artifacts
# matching package names passed to the script as parameters.
#
# The script does a simple wildcard match for packages beginning with each parameter.
# No consideration is given for dependencies.
#
# Examples:
#   pkg.sh glib - deletes glib and glib_networking
#   pkg.sh gtk poppler - deletes gtk, poppler, poppler_data
#
# Use at your own risk

root=$(git rev-parse --show-toplevel)

if [ $? -ne 0 ];
then
	echo "Git root dir not found; run this script from inside the claws-win32-installer repo"
	exit 1
fi

cd $root

while (( "$#" )); do
	rm -rf ./src/playground/build/${1}*
	rm -r ./src/playground/install/pkgs/${1}*
	rm ./src/stamps/stamp-${1}*
	shift
done

