#!/bin/bash

root=$(readlink -f "$(dirname $0)/..")
stamps="$root/src/stamps"

printf "Build progress\n--------------\n"

if [ ! -f ${root}/Makefile ]; then
	printf "No build in progress\n"
	exit 0
fi

for x in $(sed -ne 's/cm_build_list = //p' ${root}/Makefile)
do
	if [ -f ${stamps}/stamp-final-${x} ]; then
		ls -ltc --time-style="+%y-%m-%d %H:%M:%S" ${stamps}/stamp-${x}-00-* | sed -re "s;${stamps}/stamp-(.*)-00-.*;\1;" | awk '{print $6, $7, $8}'
	elif [ -f ${stamps}/stamp-${x}-00-* ]; then
		printf "\033[1m"
		ls -ltc --time-style="+%y-%m-%d %H:%M:%S" ${stamps}/stamp-${x}-00-* | sed -re "s;${stamps}/stamp-(.*)-00-.*;\1;" | awk '{print $6, $7, $8}'
		printf "\033[0m"
	else
		printf '              n/a %s\n' ${x}
	fi
done
