#!/bin/bash

# This is a helper script to completely rebuild the Claws installer.
#
# The script does a 'git clean', excluding the packages folder. After
# cleaning, you can re-run autogen.sh to continue building.
#
# Use at your own risk

root=$(git rev-parse --show-toplevel)

if [ $? -ne 0 ];
then
	echo "Git root dir not found; run this script from inside the claws-win32-installer repo"
	exit 1
fi

cd $root

echo Cleaning

git clean -dffx --exclude=/packages/

