dnl gpg4win.m4 - macros to configure gpg4win.
dnl Copyright (C) 2005, 2009 g10 Code GmbH
dnl
dnl This file is part of GPG4Win.
dnl
dnl GPG4Win is free software; you can redistribute it and/or modify
dnl it under the terms of the GNU General Public License as published by
dnl the Free Software Foundation; either version 2 of the License, or
dnl (at your option) any later version.
dnl
dnl GPG4Win is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl GNU General Public License for more details.
dnl
dnl You should have received a copy of the GNU General Public License
dnl along with this program; if not, write to the Free Software
dnl Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
dnl MA 02110-1301, USA


AC_DEFUN([CM_CHECK_DEPS],
[
	AC_MSG_CHECKING(build list)
	cm_build_list=`echo $_cm_deps | tsort`
	# Remove newlines.
	cm_build_list=`echo $cm_build_list`
	AC_MSG_RESULT($cm_build_list)
	AC_SUBST(cm_build_list)

	# Check each dependency.
	_cm_not_found=
	_cm_d=
	_cm_p=
	for _cm_p in $_cm_deps; do
		AS_IF([test -z $_cm_d],
			[_cm_d=$_cm_p],
			[
			_cm_found=
			for _cm_i in $_cm_pkgs; do
				AS_IF([test $_cm_d = $_cm_i],
					_cm_found=yes
				break)
			done
			AS_IF([test -z $_cm_found],
				AC_MSG_WARN(could not find package $_cm_d required by package $_cm_p)
				_cm_not_found=yes)
			_cm_d=
			])
	done
	AS_IF([test -n "$_cm_not_found"],
		AC_MSG_ERROR([could not find some package dependencies]))
])


# CM_PKG([PKG],[DEPENDS])
# Look up a package and record its dependencies
AC_DEFUN([CM_PKG],
[
	AC_MSG_CHECKING([for package $1])

	name=$(grep -E "^[$1]," packages/packages.current | cut -f 1 -d ,)
	AS_IF([test -z "$name"], AC_MSG_ERROR([could not find packages.current entry for $1]))

	version=$(grep -E "^$name," packages/packages.current | cut -f 2 -d ,)
	AS_IF([test -z "$version"], AC_MSG_ERROR([could not find version for $1]))

	type=$(grep -E "^$name," packages/packages.current | cut -f 3 -d ,)
	AS_IF([test -z "$type"], AC_MSG_ERROR([could not find type for $1]))

	if test "$type" = "file"; then
		url=$(grep -E "^$name," packages/packages.current | cut -f 4 -d ,)
		if test -z "$url"; then
			AC_MSG_ERROR([could not find url for $1])
		fi
		file=$(basename $url)
		if test -z "$file"; then
			AC_MSG_ERROR([could not find file for $1])
		fi
	elif test "$type" = "git"; then
		file=$name-$version.tar.xz
	fi

	file=`readlink -f packages/$file`
	AS_IF([test -r "$file"],
		AC_MSG_RESULT($file),
		[AC_MSG_RESULT([no]); AC_MSG_ERROR(could not find sources for $1)])

	AC_SUBST(cm_pkg_[$1], [$name])
	AC_SUBST(cm_pkg_[$1]_version, [$version])
	AC_SUBST(cm_pkg_[$1]_file, [$file])
	AC_SUBST(cm_pkg_[$1]_deps, "[$2]")

	_cm_pkgs="$_cm_pkgs $1"

	# Record dependencies.  Also enter every package as node.
	_cm_deps="$_cm_deps $1 $1"
	AS_IF([test -n "$2"],
		[for _cm_i in $2; do
			_cm_deps="$_cm_deps $_cm_i $1"
		done])
])

# CM_BPKG([PKG],[DEPENDS])
# Wrapper around CM_PKG for BPKGs
AC_DEFUN([CM_BPKG],
[
	cm_bpkgs="$cm_bpkgs [$1]"
	CM_PKG([$1],[$2])
])

# CM_MPKG([PKG],[DEPENDS])
# Wrapper around CM_PKG for MPKGs
AC_DEFUN([CM_MPKG],
[
	cm_mpkgs="$cm_mpkgs [$1]"
	CM_PKG([$1],[$2])
])

# CM_SPKG([PKG],[DEPENDS])
# Wrapper around CM_PKG for SPKGs
AC_DEFUN([CM_SPKG],
[
	cm_spkgs="$cm_spkgs [$1]"
	CM_PKG([$1],[$2])
])

# CM_CHECK_REQ_TOOLS([VARIABLE],[TOOL-TO-CHECK-FOR])
# Wrapper around AC_CHECK_TOOLS that errors out if the tool isn't found
AC_DEFUN([CM_CHECK_REQ_TOOLS],
[
	AC_CHECK_TOOLS([$1],[$2],[tool-not-found])
	AS_IF([test "$[]$1" = "tool-not-found"], AC_MSG_ERROR([$2 is required but could not be found]))
])

# CM_CHECK_REQ_PROGS([VARIABLE],[PROG-TO-CHECK-FOR])
# Wrapper around AC_CHECK_PROGS that errors out if the program isn't found
AC_DEFUN([CM_CHECK_REQ_PROGS],
[
	AC_CHECK_PROGS([$1],[$2],[tool-not-found])
	AS_IF([test "$[]$1" = "tool-not-found"], AC_MSG_ERROR([$2 is required but could not be found]))
])
