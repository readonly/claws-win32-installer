Section "-un.Claws-Mail"
#######################################
### claws-mail

Delete "$INSTDIR\claws-mail.exe"
Delete "$INSTDIR\claws-mail-manual.pdf"

Delete "$INSTDIR\share\claws-mail\ca-certificates.crt"
RMDir "$INSTDIR\share\claws-mail"

Delete "$INSTDIR\lib\claws-mail\plugins\address_keeper.dll"
Delete "$INSTDIR\lib\claws-mail\plugins\attachwarner.dll"
Delete "$INSTDIR\lib\claws-mail\plugins\att_remover.dll"
Delete "$INSTDIR\lib\claws-mail\plugins\bsfilter.dll"
Delete "$INSTDIR\lib\claws-mail\plugins\fetchinfo.dll"
Delete "$INSTDIR\lib\claws-mail\plugins\keyword_warner.dll"
Delete "$INSTDIR\lib\claws-mail\plugins\libravatar.dll"
Delete "$INSTDIR\lib\claws-mail\plugins\litehtml_viewer.dll"
Delete "$INSTDIR\lib\claws-mail\plugins\managesieve.dll"
Delete "$INSTDIR\lib\claws-mail\plugins\notification.dll"
Delete "$INSTDIR\lib\claws-mail\plugins\pdf_viewer.dll"
Delete "$INSTDIR\lib\claws-mail\plugins\pgpcore.dll"
Delete "$INSTDIR\lib\claws-mail\plugins\pgpinline.dll"
Delete "$INSTDIR\lib\claws-mail\plugins\pgpinline.deps"
Delete "$INSTDIR\lib\claws-mail\plugins\pgpmime.dll"
Delete "$INSTDIR\lib\claws-mail\plugins\pgpmime.deps"
Delete "$INSTDIR\lib\claws-mail\plugins\rssyl.dll"
Delete "$INSTDIR\lib\claws-mail\plugins\smime.dll"
Delete "$INSTDIR\lib\claws-mail\plugins\smime.deps"
Delete "$INSTDIR\lib\claws-mail\plugins\spamreport.dll"
Delete "$INSTDIR\lib\claws-mail\plugins\tnef_parse.dll"
Delete "$INSTDIR\lib\claws-mail\plugins\vcalendar.dll"
RMDir "$INSTDIR\lib\claws-mail\plugins"
RMDir "$INSTDIR\lib\claws-mail"

#######################################
### poppler
Delete "$INSTDIR\libpoppler-glib-8.dll"
Delete "$INSTDIR\libpoppler-146.dll"

#######################################
### gumbo-parser
Delete "$INSTDIR\libgumbo-3.dll"

#######################################
### ytnef
Delete "$INSTDIR\libytnef-0.dll"

#######################################
### libical
Delete "$INSTDIR\libical.dll"
Delete "$INSTDIR\libicalss.dll"
Delete "$INSTDIR\libicalvcal.dll"

#######################################
### nettle
Delete "$INSTDIR\libhogweed-6.dll"
Delete "$INSTDIR\libnettle-8.dll"

#######################################
### gmp
Delete "$INSTDIR\libgmp-10.dll"

#######################################
### libpsl
Delete "$INSTDIR\libpsl-5.dll"

#######################################
### jpeg
Delete "$INSTDIR\libjpeg-9.dll"

#######################################
### icu4c
Delete "$INSTDIR\icudt76.dll"
Delete "$INSTDIR\icuin76.dll"
Delete "$INSTDIR\icuuc76.dll"

#######################################
### libepoxy
Delete "$INSTDIR\libepoxy-0.dll"

### gpgme
Delete /REBOOTOK "$INSTDIR\libgpgme-11.dll"
Delete /REBOOTOK "$INSTDIR\libgpgmepp-6.dll"
Delete /REBOOTOK "$INSTDIR\libgpgme-glib-11.dll"
Delete "$INSTDIR\gpgme-w32spawn.exe"

#######################################
### libxml2
Delete "$INSTDIR\libxml2-2.dll"

#######################################
### curl
Delete "$INSTDIR\libcurl-4.dll"

#######################################
### gnutls
Delete "$INSTDIR\libgnutls-30.dll"

#######################################
### p11-kit
Delete "$INSTDIR\libp11-kit-0.dll"

#######################################
### gtk-add
# directories "etc" and "themes" are cleaned in gtk+ section
Delete "$INSTDIR\etc\gtk-3.0\settings.ini"
Delete "$INSTDIR\share\themes\win71\gtk-3.0\gtk-win32-base.css"
Delete "$INSTDIR\share\themes\win71\gtk-3.0\gtk.css"
RMDir "$INSTDIR\share\themes\win71\gtk-3.0"
RMDir "$INSTDIR\share\themes\win71"

#######################################
### adwaita-icon-theme
Delete "$INSTDIR\share\icons\Adwaita\16x16\actions\bookmark-new-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\actions\document-save-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\actions\find-location-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\actions\folder-new-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\actions\list-add-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\actions\list-remove-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\actions\media-eject-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\actions\media-playback-pause-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\actions\media-record-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\actions\object-select-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\actions\tab-new-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\actions\view-list-symbolic.symbolic.png"
RMDir "$INSTDIR\share\icons\Adwaita\16x16\actions"

Delete "$INSTDIR\share\icons\Adwaita\16x16\categories\emoji-activities-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\categories\emoji-body-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\categories\emoji-flags-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\categories\emoji-food-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\categories\emoji-nature-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\categories\emoji-objects-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\categories\emoji-people-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\categories\emoji-recent-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\categories\emoji-symbols-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\categories\emoji-travel-symbolic.symbolic.png"
RMDir "$INSTDIR\share\icons\Adwaita\16x16\categories"

Delete "$INSTDIR\share\icons\Adwaita\16x16\devices\drive-harddisk-symbolic.symbolic.png"
RMDir "$INSTDIR\share\icons\Adwaita\16x16\devices"

Delete "$INSTDIR\share\icons\Adwaita\16x16\legacy\dialog-error.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\legacy\dialog-information.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\legacy\dialog-password.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\legacy\dialog-question.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\legacy\dialog-warning.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\legacy\document-properties.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\legacy\edit-clear.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\legacy\edit-copy.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\legacy\edit-delete.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\legacy\edit-find.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\legacy\edit-redo.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\legacy\edit-undo.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\legacy\go-bottom.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\legacy\go-down.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\legacy\go-next.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\legacy\go-previous.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\legacy\go-top.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\legacy\go-up.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\legacy\help-browser.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\legacy\image-missing.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\legacy\list-add.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\legacy\list-remove.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\legacy\system-help.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\legacy\view-refresh.png"
RMDir "$INSTDIR\share\icons\Adwaita\16x16\legacy"

Delete "$INSTDIR\share\icons\Adwaita\16x16\places\folder.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\places\user-desktop-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\places\user-home-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\places\user-trash-symbolic.symbolic.png"
RMDir "$INSTDIR\share\icons\Adwaita\16x16\places"

Delete "$INSTDIR\share\icons\Adwaita\16x16\ui\checkbox-checked-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\ui\checkbox-mixed-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\ui\checkbox-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\ui\focus-legacy-systray-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\ui\focus-top-bar-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\ui\focus-windows-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\ui\list-drag-handle-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\ui\pan-down-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\ui\pan-end-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\ui\pan-end-symbolic-rtl.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\ui\pan-start-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\ui\pan-start-symbolic-rtl.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\ui\pan-up-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\ui\radio-checked-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\ui\radio-mixed-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\ui\radio-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\ui\selection-end-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\ui\selection-end-symbolic-rtl.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\ui\selection-start-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\ui\selection-start-symbolic-rtl.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\ui\window-close-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\ui\window-maximize-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\ui\window-minimize-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\ui\window-new-symbolic.symbolic.png"
Delete "$INSTDIR\share\icons\Adwaita\16x16\ui\window-restore-symbolic.symbolic.png"
RMDir "$INSTDIR\share\icons\Adwaita\16x16\ui"

RMDir "$INSTDIR\share\icons\Adwaita\16x16"

Delete "$INSTDIR\share\icons\Adwaita\32x32\mimetypes\text-x-generic.png"
RMDir "$INSTDIR\share\icons\Adwaita\32x32\mimetypes"

RMDir "$INSTDIR\share\icons\Adwaita\32x32"

Delete "$INSTDIR\share\icons\Adwaita\48x48\legacy\dialog-error.png"
Delete "$INSTDIR\share\icons\Adwaita\48x48\legacy\dialog-information.png"
Delete "$INSTDIR\share\icons\Adwaita\48x48\legacy\dialog-password.png"
Delete "$INSTDIR\share\icons\Adwaita\48x48\legacy\dialog-question.png"
Delete "$INSTDIR\share\icons\Adwaita\48x48\legacy\dialog-warning.png"
RMDir "$INSTDIR\share\icons\Adwaita\48x48\legacy"

RMDir "$INSTDIR\share\icons\Adwaita\48x48"

Delete "$INSTDIR\share\icons\Adwaita\256x256\legacy\dialog-error.png"
Delete "$INSTDIR\share\icons\Adwaita\256x256\legacy\dialog-information.png"
Delete "$INSTDIR\share\icons\Adwaita\256x256\legacy\dialog-password.png"
Delete "$INSTDIR\share\icons\Adwaita\256x256\legacy\dialog-question.png"
Delete "$INSTDIR\share\icons\Adwaita\256x256\legacy\dialog-warning.png"
RMDir "$INSTDIR\share\icons\Adwaita\256x256\legacy"

RMDir "$INSTDIR\share\icons\Adwaita\256x256"

Delete "$INSTDIR\share\icons\Adwaita\cursors\dnd-copy.cur"
Delete "$INSTDIR\share\icons\Adwaita\cursors\dnd-move.cur"
Delete "$INSTDIR\share\icons\Adwaita\cursors\dnd-none.cur"
RMDir "$INSTDIR\share\icons\Adwaita\cursors"

Delete "$INSTDIR\share\icons\Adwaita\index.theme"
Delete "$INSTDIR\share\icons\Adwaita\icon-theme.cache"

RMDir "$INSTDIR\share\icons\Adwaita"
RMDir "$INSTDIR\share\icons"

#######################################
### gtk+
Delete /REBOOTOK "$INSTDIR\libgailutil-3-0.dll"
Delete /REBOOTOK "$INSTDIR\libgdk-3-0.dll"
Delete /REBOOTOK "$INSTDIR\libgtk-3-0.dll"
Delete "$INSTDIR\gtk-query-immodules-3.0.exe"
Delete "$INSTDIR\gtk-update-icon-cache.exe"

Delete "$INSTDIR\etc\gtk-3.0\im-multipress.conf"
RMDir "$INSTDIR\etc\gtk-3.0"

Delete "$INSTDIR\share\themes\Default\gtk-3.0\gtk-keys.css"
RMDir "$INSTDIR\share\themes\Default\gtk-3.0"
RMDir "$INSTDIR\share\themes\Default"

Delete "$INSTDIR\share\themes\Emacs\gtk-3.0\gtk-keys.css"
RMDir "$INSTDIR\share\themes\Emacs\gtk-3.0"
RMDir "$INSTDIR\share\themes\Emacs"

RMDir "$INSTDIR\share\themes"

Delete "$INSTDIR\share\glib-2.0\schemas\gschemas.compiled"
Delete "$INSTDIR\share\glib-2.0\schemas\org.gtk.Settings.ColorChooser.gschema.xml"
Delete "$INSTDIR\share\glib-2.0\schemas\org.gtk.Settings.Debug.gschema.xml"
Delete "$INSTDIR\share\glib-2.0\schemas\org.gtk.Settings.EmojiChooser.gschema.xml"
Delete "$INSTDIR\share\glib-2.0\schemas\org.gtk.Settings.FileChooser.gschema.xml"
RMDir "$INSTDIR\share\glib-2.0\schemas"
RMDir "$INSTDIR\share\glib-2.0"

#######################################
### gdk-pixbuf
Delete /REBOOTOK "$INSTDIR\libgdk_pixbuf-2.0-0.dll"
Delete "$INSTDIR\gdk-pixbuf-query-loaders.exe"
Delete "$INSTDIR\gdk-pixbuf-pixdata.exe"


#######################################
### atk
Delete /REBOOTOK "$INSTDIR\libatk-1.0-0.dll"

#######################################
### pango
Delete /REBOOTOK "$INSTDIR\libpango-1.0-0.dll"
Delete /REBOOTOK "$INSTDIR\libpangoft2-1.0-0.dll"
Delete /REBOOTOK "$INSTDIR\libpangowin32-1.0-0.dll"
Delete /REBOOTOK "$INSTDIR\libpangocairo-1.0-0.dll"

#######################################
### fribidi
Delete /REBOOTOK "$INSTDIR\libfribidi-0.dll"

#######################################
### harfbuzz
Delete "$INSTDIR\libharfbuzz-0.dll"
Delete "$INSTDIR\libharfbuzz-icu-0.dll"

#######################################
### cairo
Delete /REBOOTOK "$INSTDIR\libcairo-2.dll"
Delete /REBOOTOK "$INSTDIR\libcairo-gobject-2.dll"
Delete /REBOOTOK "$INSTDIR\libcairo-script-interpreter-2.dll"

#######################################
### pixman
Delete /REBOOTOK "$INSTDIR\libpixman-1-0.dll"

#######################################
### fontconfig
Delete /REBOOTOK "$INSTDIR\libfontconfig-1.dll"
Delete "$INSTDIR\etc\fonts\fonts.conf"
RMDir "$INSTDIR\etc\fonts"

#######################################
### freetype
Delete /REBOOTOK "$INSTDIR\libfreetype-6.dll"

#######################################
### expat
Delete /REBOOTOK "$INSTDIR\libexpat-1.dll"

#######################################
### glib
Delete "$INSTDIR\share\glib-2.0\schemas\gschema.dtd"
RMDir "$INSTDIR\share\glib-2.0\schemas"
RMDir "$INSTDIR\share\glib-2.0"

Delete "$INSTDIR\gdbus.exe"
Delete "$INSTDIR\gio-querymodules.exe"
Delete "$INSTDIR\glib-compile-resources.exe"
Delete "$INSTDIR\glib-compile-schemas.exe"
Delete "$INSTDIR\glib-genmarshal.exe"
Delete "$INSTDIR\gobject-query.exe"
Delete "$INSTDIR\gresource.exe"
Delete "$INSTDIR\gsettings.exe"
!if ${nsis64} == "yes"
Delete "$INSTDIR\gspawn-win64-helper.exe"
Delete "$INSTDIR\gspawn-win64-helper-console.exe"
!else
Delete "$INSTDIR\gspawn-win32-helper.exe"
Delete "$INSTDIR\gspawn-win32-helper-console.exe"
!endif

Delete /REBOOTOK "$INSTDIR\libglib-2.0-0.dll"
Delete /REBOOTOK "$INSTDIR\libgmodule-2.0-0.dll"
Delete /REBOOTOK "$INSTDIR\libgobject-2.0-0.dll"
Delete /REBOOTOK "$INSTDIR\libgthread-2.0-0.dll"
Delete /REBOOTOK "$INSTDIR\libgio-2.0-0.dll"

RMDir "$INSTDIR\lib\gio\modules"
RMDir "$INSTDIR\lib\gio"

#######################################
### pcre2
Delete /REBOOTOK "$INSTDIR\libpcre2-8-0.dll"

#######################################
### libffi
Delete /REBOOTOK "$INSTDIR\libffi-8.dll"

#######################################
### libgpg-error
Delete /REBOOTOK "$INSTDIR\libgpg-error-0.dll"

Delete "$INSTDIR\gpg-error.exe"

#######################################
### libassuan
Delete /REBOOTOK "$INSTDIR\libassuan-9.dll"

#######################################
### cyrus-sasl
Delete /REBOOTOK "$INSTDIR\libsasl2-3.dll"
Delete /REBOOTOK "$INSTDIR\lib\sasl2\libanonymous-3.dll"
Delete /REBOOTOK "$INSTDIR\lib\sasl2\libcrammd5-3.dll"
Delete /REBOOTOK "$INSTDIR\lib\sasl2\libdigestmd5-3.dll"
Delete /REBOOTOK "$INSTDIR\lib\sasl2\liblogin-3.dll"
Delete /REBOOTOK "$INSTDIR\lib\sasl2\libplain-3.dll"
RMDir "$INSTDIR\lib\sasl2"

#######################################
### libetpan
Delete "$INSTDIR\libetpan-20.dll"

#######################################
### enchant
Delete "$INSTDIR\libenchant-2-2.dll"
Delete "$INSTDIR\lib\enchant-2\enchant_hunspell.dll"
RMDir "$INSTDIR\lib\enchant-2"

Delete "$INSTDIR\share\enchant-2\enchant.ordering"
Delete "$INSTDIR\share\hunspell\en_US.aff"
Delete "$INSTDIR\share\hunspell\en_US.dic"
Delete "$INSTDIR\share\hunspell\de_DE_frami.aff"
Delete "$INSTDIR\share\hunspell\de_DE_frami.dic"
Delete "$INSTDIR\share\hunspell\fr.aff"
Delete "$INSTDIR\share\hunspell\fr.dic"
RMDir "$INSTDIR\share\hunspell"
RMDir "$INSTDIR\share\enchant-2"

#######################################
### hunspell
Delete "$INSTDIR\libhunspell-1.7-0.dll"

#######################################
### libpng
Delete /REBOOTOK "$INSTDIR\libpng16-16.dll"

#######################################
### gettext
Delete /REBOOTOK "$INSTDIR\libintl-8.dll"

#######################################
### bsfilter
Delete /REBOOTOK "$INSTDIR\bsfilterw.exe"
Delete /REBOOTOK "$INSTDIR\bsfilter.exe"
Delete /REBOOTOK "$INSTDIR\iconv.dll"

#######################################
### zlib
Delete /REBOOTOK "$INSTDIR\zlib1.dll"

#######################################
### regex
Delete "$INSTDIR\libregex-1.dll"

#######################################
### libtasn1
Delete "$INSTDIR\libtasn1-6.dll"

#######################################
### libiconv
Delete /REBOOTOK "$INSTDIR\libiconv-2.dll"


### Final cleanup
Delete /REBOOTOK "$INSTDIR\${libstdcpp_dll}"
Delete /REBOOTOK "$INSTDIR\${libgcc_dll}"
Delete /REBOOTOK "$INSTDIR\${libwinpthread_dll}"

# Remove the locale directories.
RMDir /r "$INSTDIR\share\locale"

# Remove files that were previously distributed
Delete "$INSTDIR\intl.dll"
Delete "$INSTDIR\libwebkitgtk-1.0-0.dll"
Delete "$INSTDIR\libjavascriptcoregtk-1.0-0.dll"
Delete "$INSTDIR\libsoup-2.4-1.dll"
Delete "$INSTDIR\libsqlite3-0.dll"
Delete "$INSTDIR\libwebp-7.dll"
Delete "$INSTDIR\libxslt-1.dll"
Delete "$INSTDIR\include\assuan.h"
Delete "$INSTDIR\include\gpg-error.h"
Delete "$INSTDIR\include\gpgme.h"
Delete "$INSTDIR\lib\claws-mail\plugins\fancy.dll"
Delete "$INSTDIR\lib\gio\modules\libgiognutls.dll"
Delete "$INSTDIR\lib\libassuan.imp"
Delete "$INSTDIR\lib\libgpg-error.imp"
Delete "$INSTDIR\lib\libgpg-assuan.imp"
Delete "$INSTDIR\lib\libgpgme.imp"
Delete "$INSTDIR\lib\libgpgme-glib.imp"
RMDir "$INSTDIR\include"

# At this point, the subdirectories should be completely empty.
# We can RMDir them without /r.
RMDir "$INSTDIR\etc"
RMDir "$INSTDIR\lib"
RMDir "$INSTDIR\share"

Delete "$INSTDIR\VERSION"

SectionEnd


Section "-un.registry"
DeleteRegKey HKLM "SOFTWARE\Clients\Mail\Claws Mail"
DeleteRegKey HKLM "SOFTWARE\Classes\Claws-Mail.URL.mailto"
DeleteRegValue HKLM "SOFTWARE\RegisteredApplications" "Claws Mail"
DeleteRegKey HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\ClawsMail"
SectionEnd


Section Uninstall
# Make sure that the context of the automatic variables has been set to
# the "all users" shell folder.  This guarantees that the menu gets written
# for all users.  We have already checked that we are running as Admin; or
# we printed a warning that installation will not succeed.
SetShellVarContext all

# Start Menu
!insertmacro MUI_STARTMENU_GETFOLDER Application $MYTMP
Delete "$SMPROGRAMS\$MYTMP\*.lnk"

StrCpy $MYTMP "$SMPROGRAMS\$MYTMP"
startMenuDeleteLoop:
ClearErrors
RMDir $MYTMP
GetFullPathName $MYTMP "$MYTMP\.."
IfErrors startMenuDeleteLoopDone
StrCmp $MYTMP $SMPROGRAMS startMenuDeleteLoopDone startMenuDeleteLoop
startMenuDeleteLoopDone:

# Desktop and Quick Launch
Delete "$DESKTOP\Claws-Mail.lnk"
Delete "$QUICKLAUNCH\Claws-Mail.lnk"

Delete "$INSTDIR\claws-mail-uninstall.exe"
RMDir "$INSTDIR"

SectionEnd
