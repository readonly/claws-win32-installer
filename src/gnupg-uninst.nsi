# Clean up old GnuPG files from before 3.13.0 release where we stopped
# distributing them with Claws Mail

### GnuPG2
# Try to stop a running agent.  This is only for the admin but in
# some cases that is actually useful.
ifFileExists "$OLD_INSTDIR\gpg-connect-agent.exe" 0 no_u_gpg_connect_agent
	ifFileExists "$OLD_INSTDIR\libgpg-error-0.dll" 0 no_u_gpg_connect_agent
		ifFileExists "$OLD_INSTDIR\libw32pth-0.dll" 0 no_u_gpg_connect_agent
			ExecWait '"$OLD_INSTDIR\gpg-connect-agent.exe" killagent /bye'
no_u_gpg_connect_agent:

Delete "$OLD_INSTDIR\gpg2.exe"
Delete "$OLD_INSTDIR\gpgv2.exe"
Delete "$OLD_INSTDIR\gpgsm.exe"
Delete /REBOOTOK "$OLD_INSTDIR\gpg-agent.exe"
Delete /REBOOTOK "$OLD_INSTDIR\scdaemon.exe"
Delete "$OLD_INSTDIR\gpgconf.exe"
Delete "$OLD_INSTDIR\gpg-connect-agent.exe"
Delete "$OLD_INSTDIR\gpgsplit.exe"
Delete "$OLD_INSTDIR\gpgtar.exe"

Delete "$OLD_INSTDIR\pub\gpg.exe"
Delete "$OLD_INSTDIR\pub\gpg2.exe"
Delete "$OLD_INSTDIR\pub\gpgv.exe"
Delete "$OLD_INSTDIR\pub\gpgsm.exe"
Delete "$OLD_INSTDIR\pub\gpg-connect-agent.exe"
Delete "$OLD_INSTDIR\pub\gpgconf.exe"

Delete "$OLD_INSTDIR\gpg2keys_finger.exe"
Delete "$OLD_INSTDIR\gpg2keys_hkp.exe"
Delete "$OLD_INSTDIR\gpg2keys_curl.exe"
Delete "$OLD_INSTDIR\gpg2keys_ldap.exe"
Delete "$OLD_INSTDIR\gpg2keys_kdns.exe"
Delete "$OLD_INSTDIR\gpg-protect-tool.exe"
Delete "$OLD_INSTDIR\gpg-preset-passphrase.exe"

Delete "$OLD_INSTDIR\share\gnupg\qualified.txt"
Delete "$OLD_INSTDIR\share\gnupg\com-certs.pem"
Delete "$OLD_INSTDIR\share\gnupg\gpg-conf.skel"
Delete "$OLD_INSTDIR\share\gnupg\help.*.txt"
Delete "$OLD_INSTDIR\share\gnupg\*.man"
RMDir "$OLD_INSTDIR\share\gnupg"

Delete "$OLD_INSTDIR\share\locale\be\LC_MESSAGES\gnupg2.mo"
Delete "$OLD_INSTDIR\share\locale\ca\LC_MESSAGES\gnupg2.mo"
Delete "$OLD_INSTDIR\share\locale\cs\LC_MESSAGES\gnupg2.mo"
Delete "$OLD_INSTDIR\share\locale\da\LC_MESSAGES\gnupg2.mo"
Delete "$OLD_INSTDIR\share\locale\de\LC_MESSAGES\gnupg2.mo"
Delete "$OLD_INSTDIR\share\locale\el\LC_MESSAGES\gnupg2.mo"
Delete "$OLD_INSTDIR\share\locale\eo\LC_MESSAGES\gnupg2.mo"
Delete "$OLD_INSTDIR\share\locale\es\LC_MESSAGES\gnupg2.mo"
Delete "$OLD_INSTDIR\share\locale\et\LC_MESSAGES\gnupg2.mo"
Delete "$OLD_INSTDIR\share\locale\fi\LC_MESSAGES\gnupg2.mo"
Delete "$OLD_INSTDIR\share\locale\fr\LC_MESSAGES\gnupg2.mo"
Delete "$OLD_INSTDIR\share\locale\gl\LC_MESSAGES\gnupg2.mo"
Delete "$OLD_INSTDIR\share\locale\hu\LC_MESSAGES\gnupg2.mo"
Delete "$OLD_INSTDIR\share\locale\id\LC_MESSAGES\gnupg2.mo"
Delete "$OLD_INSTDIR\share\locale\it\LC_MESSAGES\gnupg2.mo"
Delete "$OLD_INSTDIR\share\locale\ja\LC_MESSAGES\gnupg2.mo"
Delete "$OLD_INSTDIR\share\locale\nb\LC_MESSAGES\gnupg2.mo"

### GPA
Delete /REBOOTOK "$OLD_INSTDIR\gpa.exe"
Delete /REBOOTOK "$OLD_INSTDIR\pub\gpa.exe"
RMDir "$OLD_INSTDIR\pub"

Delete "$OLD_INSTDIR\share\locale\de\LC_MESSAGES\gpa.mo"
Delete "$OLD_INSTDIR\share\locale\ar\LC_MESSAGES\gpa.mo"
Delete "$OLD_INSTDIR\share\locale\es\LC_MESSAGES\gpa.mo"
Delete "$OLD_INSTDIR\share\locale\fr\LC_MESSAGES\gpa.mo"
Delete "$OLD_INSTDIR\share\locale\ru\LC_MESSAGES\gpa.mo"

RMDir /r "$OLD_INSTDIR\share\locale"

Delete "$OLD_INSTDIR\share\gpa\gpa_tips.en"
Delete "$OLD_INSTDIR\share\gpa\gpa_tips.de"
Delete "$OLD_INSTDIR\share\gpa\gpa-logo.ppm"
Delete "$OLD_INSTDIR\share\gpa\gpa.png"
RMDir "$OLD_INSTDIR\share\gpa"

RMDir "$OLD_INSTDIR\share"

### adns
Delete "$OLD_INSTDIR\libadns-1.dll"

### libksba
Delete "$OLD_INSTDIR\libksba-8.dll"
Delete "$OLD_INSTDIR\lib\libksba.imp"
Delete "$OLD_INSTDIR\include\ksba.h"

### dirmngr
Delete "$OLD_INSTDIR\dirmngr.exe"
Delete "$OLD_INSTDIR\dirmngr-client.exe"
Delete "$OLD_INSTDIR\dirmngr_ldap.exe"
RMDir "$OLD_INSTDIR\cache"

# The next thing is only to cleanup cruft from versions < 1.9.3.
# Newer versions install them into COMMON_APPDATA and they are not
# removed on purpose.  Remove the extra-certs directory.  Obviously
# this works only if the user has not populated it.
RMDir "$OLD_INSTDIR\lib\dirmngr\extra-certs"
RMDir "$OLD_INSTDIR\lib\dirmngr"

# Remove etc files.  This is only useful for installer < 1.9.3.
Delete "$OLD_INSTDIR\etc\dirmngr\dirmngr.conf"
Delete "$OLD_INSTDIR\etc\dirmngr\bnetza-10r-ocsp.signer"

Delete "$OLD_INSTDIR\etc\dirmngr\trusted-certs\README"
Delete "$OLD_INSTDIR\etc\dirmngr\trusted-certs\bnetza-10r-ca.crt"

RMDir "$OLD_INSTDIR\etc\dirmngr\trusted-certs"
RMDir "$OLD_INSTDIR\etc\dirmngr"
RMDir "$OLD_INSTDIR\etc"

### pinentry
Delete "$OLD_INSTDIR\pinentry-w32.exe"
Delete "$OLD_INSTDIR\pinentry-gtk-2.exe"
Delete "$OLD_INSTDIR\pinentry-qt4.exe"
Delete "$OLD_INSTDIR\pinentry.exe"

RMDir "$OLD_INSTDIR"
