#!/bin/sh

# 50-win71-theme-prepare.patch

# Patches 51..57: Taken from Jamie Velasco / https://gitlab.gnome.org/GNOME/gtk/-/issues/687
# Original numbering changed from 0001..0007 to 51..57
# Further patches added by Thorsten Maerz for Claws-Mail
# These patches are meant to directly modify the compiled-in win32 theme

# To keep gtk unchanged, the original files are backed up first.
# The finalize.patch copies the modified files to a new location,
# adds a settings file and finally restores the old directory before build.

cd gtk/theme || exit 1
cp -a win32 win32.bak || exit 1
