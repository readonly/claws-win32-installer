diff -aur a/gtk/theme/win32/gtk.css b/gtk/theme/win32/gtk.css
--- a/gtk/theme/win32/gtk.css	2021-08-03 16:41:08.501811024 -0700
+++ b/gtk/theme/win32/gtk.css	2021-08-15 03:25:38.858490703 -0700
@@ -1,3 +1,9 @@
+/* win71 theme
+ * Based on (internal) win32 theme from gtk+
+ * Additional patches from Jaime Velasco: https://gitlab.gnome.org/GNOME/gtk/-/issues/687
+ * Additional changes from Thorsten Maerz <info@netztorte.de>
+ */
+
 @import url("gtk-win32-base.css");
 
 /* Menus */
@@ -133,8 +139,7 @@
   margin: calc(0px - -gtk-win32-size(window, cyframe) - -gtk-win32-size(window, cxpaddedborder))
           calc(0px - -gtk-win32-size(window, cxframe) - -gtk-win32-size(window, cxpaddedborder));
   margin-bottom: 0px;
-  padding: calc(-gtk-win32-size(window, cyframe) + -gtk-win32-size(window, cxpaddedborder))
-           calc(-gtk-win32-size(window, cxframe) + -gtk-win32-size(window, cxpaddedborder));
+  padding: 0px 4px; /* Titlebar took too much space */
   padding-bottom: 0px;
   background: -gtk-win32-theme-part(window, 1, 1);
   min-height: calc(-gtk-win32-size(window, cycaption) - -gtk-win32-size(window, cyframe) - -gtk-win32-size(window, cxpaddedborder));
@@ -272,3 +277,153 @@
 .maximized .titlebar button.maximize:disabled {
     background-image: -gtk-win32-theme-part(window, 21, 4);
 }
+
+/* Draw missing arrows for submenu entries */
+menu menuitem arrow, .menu menuitem arrow {
+	min-width: 16px;
+	min-height: 16px;
+}
+
+menu menuitem arrow, .menu menuitem arrow {
+	margin-left: 10px;
+	-gtk-icon-source: -gtk-win32-theme-part(menu, 16, 1);
+}
+
+/* Draw missing separator between menu entries */
+menu separator {
+	padding: 0px;
+	background-color: shade(@bg_color, 0.75);
+}
+
+/* Draw native +/- expanders in treeview (instead of arrows) */
+treeview.view.expander:checked {
+	-gtk-icon-source: -gtk-win32-theme-part(treeview, 2, 2);
+}
+
+treeview.view.expander {
+	-gtk-icon-source: -gtk-win32-theme-part(treeview, 2, 1);
+}
+
+/* Unfocused treeview selections are hardly visible on MSWin
+ * Use background color instead of light grey (e.g. selected folder
+ * should stay visible, even when focus is changed to a different
+ * widget, e.g. a file selection widget for that folder).
+ * This isn't native MSWin behaviour, but rather resembles the
+ * behaviour of Posix based applications. */
+treeview.view:selected {
+	background-color: @selected_bg_color;
+}
+
+/* Combobox had too little height */
+combobox {
+	margin-top: 1px;
+	min-height: -gtk-win32-size(combobox, cysize);
+}
+
+/* Make the toolbar separators visible */
+toolbar.horizontal separator {
+	margin: 8px 3px;
+	min-width: 1px;
+	background-color: shade(@bg_color, 0.75);
+}
+
+/* Remove extra border in scrolled windows */
+scrolledwindow.frame {
+	border: 0px;
+}
+
+/* Remove extra border in textview */
+textview {
+	background-image: none;
+}
+
+/* Remove extra padding around scrollbars */
+scrollbar.vertical {
+	margin-left: -3px;
+}
+
+scrollbar.horizontal {
+	margin-top: -3px;
+}
+
+/* Silence Gtk-WARNING message: for_size smaller than min-size */
+spinbutton.horizontal button {
+	min-width: 0px;
+}
+
+/* Make folderview and summaryview background white */
+#folderview,
+#summaryview > notebook > stack > scrolledwindow  {
+	background-color: white;
+}
+
+/* Shift folderview and summaryview vertical scrollbars up to
+ * fill the corner between the scrollbar and column headers */
+#folderview > scrollbar.vertical,
+#summaryview > notebook > stack > scrolledwindow > scrollbar.vertical {
+	margin-top: -24px;
+}
+
+/* Make folderview and summaryview column headers closer together */
+#folderview_sctree > button,
+#summaryview_sctree > button {
+	margin-left: 0px;
+	margin-right: -1px;
+}
+
+/* Fix status bar height bouncing when progress bar toggles visibility */
+#hbox_stat > progressbar.horizontal > trough,
+#hbox_stat > progressbar.horizontal > trough > progress {
+	min-height: 9px;
+}
+
+/* Fix messageview avatar offset */
+#textview_avatar,
+#textview_contact_pic {
+	margin-top: -3px;
+	margin-left: -13px;
+}
+
+/* Fix messageview attachment icon offset */
+#textview_icon {
+	margin-left: -13px;
+}
+
+/* Remove extra border around pdf viewer buttons */
+#mime_notebook > stack {
+	background-image: none;
+}
+
+/* Fix pdf viewer button size */
+#pdf_viewer > #buttons button {
+	min-height: 24px;
+	min-width: 26px;
+}
+
+/* Fix pdf viewer spin button size */
+#pdf_viewer > #buttons spinbutton.horizontal button {
+	min-width: 0px;
+
+	background-size: 15px 15px;
+	background-repeat: no-repeat;
+}
+
+#pdf_viewer > #buttons spinbutton.horizontal button.down {
+	margin-top: 14px;
+	margin-bottom: -14px;
+}
+
+#pdf_viewer > #buttons spinbutton.horizontal button.up {
+	margin-top: -14px;
+	margin-bottom: 14px;
+}
+
+/* Set mimeviewer background color */
+#mime_notebook viewport {
+	background-color: white;
+}
+
+/* Fix status bar height bouncing when text toggles visibility */
+#hbox_stat > statusbar label {
+	min-height: 17px;
+}
