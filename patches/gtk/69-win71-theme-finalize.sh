#!/bin/sh

# 69-win71-theme-finalize.patch
# Restore files backed up by 50-win71-theme-prepare.patch

cd gtk/theme || exit 1

mkdir claws || exit 1
mv -n win32 claws/win71 || exit 1
mv -n win32.bak win32 || exit 1

cat << GTK_INI_EOF > claws/settings.ini
[Settings]
gtk-theme-name=win71
gtk-auto-mnemonics=true
gtk-button-images=true
#gtk-application-prefer-dark-theme=1

[Doc]
#= To enable CSD, set following environment variable
#=   GTK_CSD=1
GTK_INI_EOF
