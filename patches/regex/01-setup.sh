#!/bin/bash

pkg_sdir="$(readlink -f $(pwd))"
srcdir="$(readlink -f $(dirname $0)/../../src)"

# The regex-xyz.tar.xz package is a full gnulib source archive. We move the
# gnulib sources to the 'gnulib' subdirectory and copy our regex dll-building
# files to the ${pkg_sdir}

gnulib="$(mktemp -d ../gnulib-XXXX)"
shopt -s dotglob
mv -n ${pkg_sdir}/* ${gnulib}/ || exit 1
shopt -u dotglob
cp -a ${srcdir}/regex/* ${pkg_sdir} || exit 1
mv ${gnulib} ${pkg_sdir}/gnulib || exit 1
cd ${pkg_sdir} || exit 1

./gnulib/gnulib-tool --lgpl=2 --libtool --local-dir=local --import regex || exit 1

sed -i 's/^AM_CPPFLAGS =/AM_CPPFLAGS = -D_REGEX_RE_COMP/' lib/Makefile.am || exit 1

autoreconf -fvi || exit 1
