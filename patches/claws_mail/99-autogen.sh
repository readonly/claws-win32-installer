#!/bin/sh

packages="$(readlink -f $(dirname $0)/../../packages/packages.current)"

claws_type=$(grep -E "^claws_mail," ${packages} | cut -f 3 -d ,)
if [ "x${claws_type}" != "xgit" ]; then
	exit 0
fi

NOCONFIGURE=1 ./autogen.sh

