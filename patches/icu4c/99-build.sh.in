#!/bin/bash

buildroot="$(readlink -f $(dirname $0)/../..)"
srcdir="${buildroot}/src"
builddir="${srcdir}/playground/build"
incdir="${srcdir}/playground/install/include"
packagedir="${buildroot}/packages"
patchdir="${buildroot}/patches"
icu_name="$(basename $(dirname $0))"
icu_ver="$(grep "^${icu_name}," ${packagedir}/packages.current | cut -f 2 -d ,)"
icu="${icu_name}-${icu_ver}"
prefix="${srcdir}/playground/install/pkgs/${icu}"

cd ${builddir}
mkdir ${icu}-linux
mkdir ${icu}-build

# Build for host
cd ${builddir}/${icu}-linux
sh ${builddir}/${icu}/source/runConfigureICU Linux/gcc
ret=$?
if [ $ret -ne 0 ]; then
	echo configure for host ICU build failed
	exit $ret
fi

make -j@CM_MAKE_JOBS@
ret=$?
if [ $ret -ne 0 ]; then
	echo make for host ICU build failed
	exit $ret
fi

# Configure for Windows
cd ${builddir}/${icu}
shopt -s nullglob
for pfile in ${patchdir}/${icu}/*.patch.linux ${patchdir}/${icu_name}/*.patch.linux; do
	patch -p1 < ${pfile} || exit 1
done
shopt -u nullglob

cd ${builddir}/${icu}-build
sh ${builddir}/${icu}/source/configure \
	--prefix=${prefix} \
	--host=@host@ \
	--build=@build@ \
	--with-cross-build=${builddir}/${icu}-linux \
	CPPFLAGS="-I${incdir}" \
	CFLAGS="@CM_CFLAGS@" \
	CXXFLAGS="@CM_CXXFLAGS@"
ret=$?
if [ $ret -ne 0 ]; then
	echo configure for Windows ICU build failed
	exit $ret
fi

touch ${srcdir}/stamps/stamp-icu4c-02-configure
