FROM debian:bookworm
RUN echo "deb http://deb.debian.org/debian bookworm-backports main" >> /etc/apt/sources.list \
    && apt-get update && apt-get -y install \
    build-essential \
    automake \
    autoconf \
    mingw-w64 \
    mingw-w64-tools \
    nsis \
    stow \
    unzip \
    docbook-utils \
    libglib2.0-bin \
    libglib2.0-dev-bin \
    git \
    cmake \
    bison \
    flex \
    gperf \
    intltool \
    libtool \
    libgettextpo-dev \
    meson/bookworm-backports \
    curl \
    libgtk-3-bin \
    python3-packaging \
    && rm -rf /var/lib/apt/lists/* \
    && update-alternatives --set i686-w64-mingw32-g++ /usr/bin/i686-w64-mingw32-g++-posix \
    && update-alternatives --set i686-w64-mingw32-gcc /usr/bin/i686-w64-mingw32-gcc-posix \
    && update-alternatives --set x86_64-w64-mingw32-g++ /usr/bin/x86_64-w64-mingw32-g++-posix \
    && update-alternatives --set x86_64-w64-mingw32-gcc /usr/bin/x86_64-w64-mingw32-gcc-posix
